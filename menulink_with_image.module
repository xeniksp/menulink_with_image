<?php

/**
 * @file
 * menulink_with_image.module
 */

use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\image\Entity\ImageStyle;

/**
 * Implements hook_entity_base_field_info().
 */
function menulink_with_image_entity_base_field_info(EntityTypeInterface $entity_type) {
  if ($entity_type->id() == 'menu_link_content') {
    $fields = [];
    $fields['menu_image'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Menu image'))
      ->setDescription(t('An image to come along the menu link'))
      ->setSetting('target_type', 'media')
      ->setSetting('handler_settings', ['target_bundles' => ['image' => 'image']])
      ->setCardinality(1)
      ->setTranslatable(FALSE)
      ->setCustomStorage(FALSE)
      ->setDisplayOptions('form', [
        'weight' => 1,
        'type' => 'inline_entity_form_complex',
        'settings' => [
          'form_mode' => 'default',
          'allow_new' => TRUE,
          'allow_existing' => TRUE,
          'match_operator' => 'CONTAINS',
          'override_labels' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);
    return $fields;
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function menulink_with_image_preprocess_menu__menu_with_image(&$variables) {
  foreach ($variables['items'] as $key => $item) {
    $uuid = $item['original_link']->getDerivativeId();
    $entity = \Drupal::entityManager()->loadEntityByUuid('menu_link_content', $uuid);
    if ($mid = $entity->get('menu_image')->getValue()[0]['target_id']) {
      // We load the Media entity.
      $media_image = Media::load($mid);
      $media_image_values = $media_image->get('field_media_image')[0]->getValue();
      // We load the File entity.
      $image_url = File::load($media_image_values['target_id']);
      $image_alt = $media_image_values['alt'];
      if ($image_url) {
        $menu_image_url = $image_url->getFileUri();
        // Load the wanted image style and generate the url.
        $style = ImageStyle::load('medium');
        $menu_image_style_url = file_create_url($style->buildUri($menu_image_url));
        // We create the image.
        $menu_image = [
          '#theme' => 'image',
          '#attributes' => [
            'src' => $menu_image_style_url,
            'alt' => $image_alt,
          ],
        ];
        // We change the title from string to array and render the image inside.
        $variables['items'][$key]['title'] = [];
        $variables['items'][$key]['title']['#markup'] = \Drupal::service('renderer')->render($menu_image);
        // A helper css class for the li.
        $variables['items'][$key]['attributes']->setAttribute('class', ['link-has-image']);
        // Just add that to avoid css :)
        $variables['items'][$key]['attributes']->setAttribute('style', 'display:inline-block');
      }
    }
  }
}
